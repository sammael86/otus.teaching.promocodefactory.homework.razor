using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PromoCodeMapper
    {
        public static PromoCodeShortResponse MapFromModelToShortResponse(PromoCode promoCode)
        {
            return new PromoCodeShortResponse()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                BeginDate = promoCode.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = promoCode.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo
            };
        }

        public static GivePromoCodeRequest MapFromModelToPromoCodeRequest(List<EmployeeShortResponse> employees,
            List<PreferenceResponse> preferences, PromoCode promoCode = null)
        {
            var request = new GivePromoCodeRequest()
            {
                Id = promoCode?.Id ?? new Guid(),
                PromoCode = promoCode?.Code ?? "",
                ServiceInfo = promoCode?.ServiceInfo ?? "",
                BeginDate = promoCode?.BeginDate ?? DateTime.Today,
                EndDate = promoCode?.EndDate ?? DateTime.Today.AddDays(30),
                Employees = employees,
                Preferences = preferences
            };

            if (promoCode?.PartnerManager is not null)
                request.PartnerId = promoCode.PartnerManager.Id;

            if (promoCode?.Preference.Id is not null)
                request.PreferenceId = promoCode.Preference.Id;

            return request;
        }

        public static PromoCode MapFromPromoCodeRequestToModel(GivePromoCodeRequest request, Employee employee,
            Preference preference)
        {
            employee.AppliedPromocodesCount++;
            return new PromoCode()
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                BeginDate = request.BeginDate,
                EndDate = request.EndDate,
                PartnerManager = employee,
                PartnerName = employee.FullName,
                Preference = preference
            };
        }

        public static void MapFromPromoCodeEditRequestToModel(this PromoCode promoCode, GivePromoCodeRequest request,
            Employee employee, Preference preference)
        {
            promoCode.Code = request.PromoCode;
            promoCode.ServiceInfo = request.ServiceInfo;
            promoCode.BeginDate = request.BeginDate;
            promoCode.EndDate = request.EndDate;
            promoCode.Preference = preference;
            promoCode.PartnerManager = employee;
            promoCode.PartnerName = employee.FullName;
        }
    }
}