using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class EmployeeMapper
    {
        public static EmployeeShortResponse MapFromModelToShortResponse(Employee employee)
        {
            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Email = employee.Email
            };
        }
    }
}