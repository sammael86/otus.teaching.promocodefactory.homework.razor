using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PreferenceMapper
    {
        public static void MapFromRequestToModel(this Preference preference, Preference preferenceRequest)
        {
            preference.Name = preferenceRequest.Name;
        }

        public static PreferenceResponse MapFromModelToResponse(Preference preference)
        {
            return new PreferenceResponse()
            {
                Id = preference.Id,
                Name = preference.Name
            };
        }
    }
}