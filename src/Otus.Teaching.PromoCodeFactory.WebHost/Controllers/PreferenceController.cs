using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class PreferenceController : Controller
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        // GET: Preference
        public async Task<IActionResult> Index()
        {
            return View(await _preferenceRepository.GetAllAsync());
        }

        // GET: Preference/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                return NotFound();

            return View(preference);
        }

        // GET: Preference/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Preference/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] Preference preference)
        {
            if (!ModelState.IsValid)
                return View(preference);

            await _preferenceRepository.AddAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        // GET: Preference/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                return NotFound();

            return View(preference);
        }

        // POST: Preference/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Name,Id")] Preference request)
        {
            if (id != request.Id)
                return NotFound();

            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                return NotFound();

            if (!ModelState.IsValid)
                return View(request);
            
            preference.MapFromRequestToModel(request);

            await _preferenceRepository.UpdateAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        // GET: Preference/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            if (preference == null)
                return NotFound();

            return View(preference);
        }

        // POST: Preference/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);
            await _preferenceRepository.DeleteAsync(preference);

            return RedirectToAction(nameof(Index));
        }
    }
}