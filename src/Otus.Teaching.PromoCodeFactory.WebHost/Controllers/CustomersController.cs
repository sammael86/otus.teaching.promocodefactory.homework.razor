using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(ICustomerRepository customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        // GET: Customer
        public async Task<IActionResult> Index()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers
                .Select(CustomerMapper.MapFromCustomerToModel)
                .ToList();

            return View(response);
        }

        // GET: Customer/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var response = new CustomerResponse(customer);

            return View(response);
        }

        // GET: Customer/Create
        public async Task<IActionResult> Create()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferencesResponses = preferences
                .Select(p => new PreferenceResponse
                {
                    Id = p.Id,
                    Name = p.Name
                })
                .ToList();

            var customer = new Customer();

            var response = CustomerMapper
                .MapFromCustomerToEditRequestModel(customer, preferencesResponses);

            return View(response);
        }

        // POST: Customer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("FirstName,LastName,Email,PreferenceIds")] CreateOrEditCustomerRequest request)
        {
            if (!ModelState.IsValid)
                return View(request);

            var preferences = await GetPreferencesAsync(request.PreferenceIds);

            var customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);

            return RedirectToAction(nameof(Index));
        }

        // GET: Customer/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferencesResponses = preferences
                .Select(p => new PreferenceResponse
                {
                    Id = p.Id,
                    Name = p.Name
                })
                .ToList();

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var response = CustomerMapper
                .MapFromCustomerToEditRequestModel(customer, preferencesResponses);

            return View(response);
        }

        // POST: Customer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id,
            [Bind("FirstName,LastName,Email,Id,PreferenceIds")]
            CreateOrEditCustomerRequest request)
        {
            if (id != request.Id)
                return NotFound();

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var preferences = await GetPreferencesAsync(request.PreferenceIds);

            if (!ModelState.IsValid)
                return View(request);

            CustomerMapper.MapFromModel(request, preferences, customer);
            await _customerRepository.UpdateAsync(customer);

            return RedirectToAction(nameof(Index));
        }

        // GET: Customer/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            return View(customer);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            await _customerRepository.DeleteAsync(customer);

            return RedirectToAction(nameof(Index));
        }

        private Task<IEnumerable<Preference>> GetPreferencesAsync(IEnumerable<Guid> ids)
        {
            IEnumerable<Preference> preferences = new List<Preference>();
            if (ids != null && ids.Any())
            {
                return _preferenceRepository
                    .GetRangeByIdsAsync(ids.ToList());
            }

            return Task.FromResult(preferences);
        }
    }
}