using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class PromoCodeController : Controller
    {
        private readonly IPromoCodeRepository _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;

        public PromoCodeController(IPromoCodeRepository promoCodeRepository,
            IRepository<Preference> preferenceRepository, IRepository<Employee> employeeRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _employeeRepository = employeeRepository;
        }

        // GET: PromoCode
        public async Task<IActionResult> Index()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();
            var response = promocodes.Select(PromoCodeMapper.MapFromModelToShortResponse);

            return View(response);
        }

        // GET: PromoCode/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            if (promoCode == null)
                return NotFound();

            var response = PromoCodeMapper.MapFromModelToShortResponse(promoCode);

            return View(response);
        }

        // GET: PromoCode/Create
        public async Task<IActionResult> Create()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var partners = await _employeeRepository.GetAllAsync();

            var preferencesResponse = preferences
                .Select(PreferenceMapper.MapFromModelToResponse)
                .ToList();
            var partnersResponse = partners
                .Select(EmployeeMapper.MapFromModelToShortResponse)
                .ToList();

            var response = PromoCodeMapper
                .MapFromModelToPromoCodeRequest(partnersResponse, preferencesResponse);

            return View(response);
        }

        // POST: PromoCode/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("PromoCode,ServiceInfo,BeginDate,EndDate,PartnerId,PreferenceId")]
            GivePromoCodeRequest request)
        {
            if (!ModelState.IsValid)
                return View(request);

            var employee = await _employeeRepository.GetByIdAsync(request.PartnerId);
            if (employee == null)
                return NotFound();

            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);
            if (preference == null)
                return NotFound();

            var promoCode = PromoCodeMapper.MapFromPromoCodeRequestToModel(request, employee, preference);

            await _promoCodeRepository.AddAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCode/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            if (promoCode == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetAllAsync();
            var partners = await _employeeRepository.GetAllAsync();

            var preferencesResponse = preferences
                .Select(PreferenceMapper.MapFromModelToResponse)
                .ToList();
            var partnersResponse = partners
                .Select(EmployeeMapper.MapFromModelToShortResponse)
                .ToList();

            var response = PromoCodeMapper
                .MapFromModelToPromoCodeRequest(partnersResponse, preferencesResponse, promoCode);

            return View(response);
        }

        // POST: PromoCode/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id,
            [Bind("PromoCode,ServiceInfo,BeginDate,EndDate,PartnerId,PreferenceId,Id")]
            GivePromoCodeRequest request)
        {
            if (id != request.Id)
                return NotFound();

            if (!ModelState.IsValid)
                return View(request);

            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            if (promoCode == null)
                return NotFound();

            var employee = await _employeeRepository.GetByIdAsync(request.PartnerId);
            if (employee == null)
                return NotFound();

            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);
            if (preference == null)
                return NotFound();

            promoCode.MapFromPromoCodeEditRequestToModel(request, employee, preference);

            await _promoCodeRepository.UpdateAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCode/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            if (promoCode == null)
                return NotFound();

            return View(promoCode);
        }

        // POST: PromoCode/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var promoCode = await _promoCodeRepository.GetByIdAsync(id);
            await _promoCodeRepository.DeleteAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }
    }
}