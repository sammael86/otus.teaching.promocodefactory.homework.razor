﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public Guid Id { get; set; }
        public string PromoCode { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PreferenceId { get; set; }

        public List<EmployeeShortResponse> Employees { get; set; }

        public List<PreferenceResponse> Preferences { get; set; }
    }
}