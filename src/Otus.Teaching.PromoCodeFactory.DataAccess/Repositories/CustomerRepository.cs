using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var entity = await DataContext.Customers
                .Include(c=>c.Preferences)
                .ThenInclude(p=>p.Preference)
                .FirstOrDefaultAsync(x => x.Id == id);

            return entity;
        }
    }
}