using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromoCodeRepository : EfRepository<PromoCode>, IPromoCodeRepository
    {
        public PromoCodeRepository(DataContext dataContext) : base(dataContext)
        {
        }

        public override async Task<PromoCode> GetByIdAsync(Guid id)
        {
            return await DataContext.PromoCodes
                .Include(p => p.PartnerManager)
                .Include(p => p.Preference)
                .FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}